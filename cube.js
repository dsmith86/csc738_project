var Cube = (function() {

	var steps = [];
	var phase;
	var currentState = new Int32Array(40);
	var goalState = new Int32Array(40);

	var applicableMoves = [ 0, 262143, 259263, 74943, 74898 ];

	var affectedCubies = [
		[0,  1,  2,  3,  0,  1,  2,  3],
		[4,  7,  6,  5,  4,  5,  6,  7],
		[0,  9,  4,  8,  0,  3,  5,  4],
		[2, 10,  6, 11,  2,  1,  7,  6],
		[3, 11,  7,  9,  3,  2,  6,  5],
		[1,  8,  5, 10,  1,  0,  4,  7]
	];

	var id = function(state) {
		switch (phase) {
			case 1:
				return state.slice().subarray(20, 32);
			case 2:
				var result = state.slice().subarray(31, 40);
				for (var e = 0; e < 12; e++) {
					result[0] |= (state[e] / 8) << e;
				}
				return result;
			case 3:
				var result = new Int32Array(3);
				for (var e = 0; e < 12; e++) {
					result[0] |= ((state[e] > 7) ? 2 : (state[e] & 1)) << (2*e);
				}
				for (var c = 0; c < 8; c++) {
					result[1] |= ((state[c+12]-12) & 5) << (3*c);
				}
				for (var i = 12; i < 20; i++) {
					for (var j = i + 1; j < 20; j++) {
						result[2] ^= state[i] > state[j];
					}
				}
				return result;
			default:
				return state;
		}
	};

	function inverse(move){
		return move + 2 - 2 * (move % 3);
	}

	function applyMove(move, state){
		var turns = move % 3 + 1;
		var face = Math.floor(move / 3);
		var newState = state.slice();

		while( turns-- ){
			var oldState = newState.slice();
			for(var i=0; i<8; i++){
				var isCorner = i > 3;
				var target = affectedCubies[face][i] + isCorner*12;
				var killer = affectedCubies[face][(i & 3) === 3 ? i - 3 : i + 1] + isCorner*12;
				var orientationDelta = (i < 4) ? (face > 1 && face < 4) : (face < 2) ? 0 : 2 - (i & 1);
				newState[target] = oldState[killer];
				newState[target + 20] = oldState[killer + 20] + orientationDelta;

				if(!turns)
					newState[target + 20] %= 2 + isCorner;
			}
		}

		return newState;
	}

	function Cube(state) {
		console.log("Initial state:");
		console.log(state + "\n");

		state = state.split(",");

		steps = [];

		var goal = ["UF", "UR", "UB", "UL", "DF", "DR", "DB", "DL", "FR", "FL", "BR", "BL", "UFR", "URB", "UBL", "ULF", "DRF", "DFL", "DLB", "DBR"];

		for (var i = 0; i < 20; i++) {
			goalState[i] = i;

			var cubie = state[i];

			currentState[i] = goal.indexOf(cubie);

			while (currentState[i] === -1) {
				cubie = cubie.substring(1) + cubie.charAt(0);
				currentState[i + 20]++;

				currentState[i] = goal.indexOf(cubie);
			}
		}

		// Setup state etc
	}

	Cube.prototype.solve = function() {
		var treeSize = 0;
		var startTime = new Date();

		for (phase = 1; phase < 5; phase++) {
			var phaseTime = new Date();
			var phaseSteps = 0;

			var currentId = id(currentState);
			var goalId = id(goalState);
			
			if (currentId.toString() === goalId.toString()) {
				continue;
			}

			var q = [];
			q.push(currentState);
			q.push(goalState);

			var predecessor = {};
			var direction = {};
			var lastMove = {};

			direction[currentId] = 1;
			direction[goalId] = 2;

			
			

			var flag = true;
			var count = 0;

			//for(var i = 0; i < 1; i ++){
			while(flag){
				
				var oldState = q.shift();
				var oldId = id(oldState);
				
				
				
				var oldDir = direction[oldId];

				for(var move=0; move<18; move++){
					count++;

					if(applicableMoves[phase] & (1 << move)){

						var newState = applyMove(move, oldState);

						var newId = id(newState);
						
						var newDir = direction[newId];

						if (!newDir) {
							newDir = 0;
						}
						if (!oldDir) {
							oldDir = 0;
						}

						if(newDir && newDir != oldDir){

							if(oldDir > 1){
								var tempId = oldId;
								oldId = newId;
								newId = tempId;

								move = inverse(move);
							}

							var algorithm = [move];
							while(oldId.toString() != currentId.toString()){
								//console.log("predecessor count: " + Object.keys(predecessor).length);
								//console.log("count: " + count);
								algorithm.splice(0, 0, lastMove[oldId]);
								oldId = predecessor[oldId];
							}

							while(newId.toString() != goalId.toString()){
								algorithm.push(inverse(lastMove[newId]));
								newId = predecessor[newId];
							}

							for(var i=0; i<algorithm.length; i++){
								var face = "UDFBLR"[Math.floor(algorithm[i]/3)];
								var turns = algorithm[i]%3+1;

								switch (turns) {
								case 1:
									turns = "";
									break;
								case 2:
									turns = "2";
									break;
								case 3:
									turns = "'";
									break;
								default:
									break;
								}

								steps.push(face + turns);

								currentState = applyMove(algorithm[i], currentState);
							}

							phaseSteps = algorithm.length;
	
							flag = false;
							break;

						} else if(!newDir){
							q.push(newState);

							newDir = oldDir;
							direction[newId] = newDir;
							lastMove[newId] = move;
							predecessor[newId] = oldId;
						}
					}
				}
			}

			treeSize += Object.keys(predecessor).length;

			console.log("Phase " + phase + " statistics:");
			console.log('Size of tree: ' + Object.keys(predecessor).length);
			console.log("Runtime: " + (new Date() - phaseTime) / 1000 + " seconds");
			console.log("Number of steps: " + phaseSteps);
			console.log();
		}

		console.log("Overall statistics:");
		console.log("Total tree size: " + treeSize);
		console.log("Total runtime: " + (new Date() - startTime) / 1000 + " seconds");
		console.log("Number of steps: " + this.numSteps());

		console.log();
		console.log("Solution: ");
		console.log(this.solution());
	}

	Cube.prototype.solution = function() {
		// list of steps in Singmaster notation separated by a space

		return steps.join(",");
	}

	Cube.prototype.numSteps = function() {
		return steps.length;
	}

	return Cube;

}());

module.exports = Cube;
