var express = require('express');
var bodyParser = require('body-parser');


//console-stamp used to log times of logs
var consoleStamp = require('console-stamp')(console, { pattern : "dd/mm/yy HH:MM:ss"} );

var https = require('https');
var http = require('http');

var app = express();
app.set('json spaces', 2);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var router = express.Router();

//REGISTER OUR ROUTES
app.use('/cube', router);

router.get('/', function(req, res) {
	res.json({ message: 'Welcome to the API!' });
});

router.route('/solver/:state')

	.get(function(req, res) {
		var Cube = require("./cube.js");

		var startTime = new Date();

		var cube = new Cube(req.params.state);

		cube.solve();

		var endTime = (new Date() - startTime) / 1000.0;

		res.send({
			time: endTime,
			steps: cube.numSteps(),
			solution: cube.solution()
		});

	});

//get some server metadata
app.get('/metadata', function(req, res) {
    res.send(client.hosts.slice(0).map(function (node) {
        return { address : node.address, rack : node.rack, datacenter : node.datacenter }
    }));
});

//Because this server is running on a VM, it was required that I specifically told it to listen on it's localIP.
//This took my whole life to solve...
http.createServer(app).listen(8888);

app.listen = function() {
  var server = http.createServer(this);
  return server.listen.apply(server, arguments);
};